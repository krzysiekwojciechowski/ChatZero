package com.kwojciech.zero.ChatZero.model;

import javax.persistence.*;

@Entity
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String text;
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
    @JoinColumn(name = "customer_id")
    private User author;
    private String tags;

    public Message() {
    }

    public Message(String text, String tags, int id, User author) {
        this.text = text;
        this.tags = tags;
        this.id = id;
        this.author = author;
    }

    public Message(String text, String tags, User author) {
        this.text = text;
        this.tags = tags;
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "Message{" +
                "text='" + text + '\'' +
                ", tags='" + tags + '\'' +
                ", id=" + id +
                ", author=" + author +
                '}';
    }
}
