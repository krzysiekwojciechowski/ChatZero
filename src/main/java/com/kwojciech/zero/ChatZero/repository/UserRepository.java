package com.kwojciech.zero.ChatZero.repository;

import com.kwojciech.zero.ChatZero.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, String> {
    List<User> findAll();
    Optional<User> findAllByLogin(String login);
    boolean existsByLogin(String login);
}
