package com.kwojciech.zero.ChatZero.repository;

import com.kwojciech.zero.ChatZero.model.Message;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessagesRepository extends CrudRepository<Message, Integer> {
    List<Message> findAll();
}
