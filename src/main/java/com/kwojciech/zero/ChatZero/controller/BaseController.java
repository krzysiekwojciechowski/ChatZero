package com.kwojciech.zero.ChatZero.controller;

import com.kwojciech.zero.ChatZero.controller.Exception.UserNotFound;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

public class BaseController {
    /*
   Exception hadler - automatycznie zamienia każdy wyjątek
   UserNotFound na kod odpowiedzi 404 NOT_FOUND
*/
    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "User not found!")
    @ExceptionHandler(UserNotFound.class)
    public void handleUserNotFound() {
    }

}
