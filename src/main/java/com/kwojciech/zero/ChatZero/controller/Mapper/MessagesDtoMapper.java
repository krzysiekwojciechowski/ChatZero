package com.kwojciech.zero.ChatZero.controller.Mapper;

import com.kwojciech.zero.ChatZero.controller.dto.CreateMessageDTO;
import com.kwojciech.zero.ChatZero.controller.dto.MessageDTO;
import com.kwojciech.zero.ChatZero.model.Message;
import org.springframework.stereotype.Component;

@Component
public class MessagesDtoMapper {
    public MessageDTO toDto(Message massage) {
        MessageDTO messageDTO = new MessageDTO();

        messageDTO.setAuthor(messageDTO.getAuthor());
        messageDTO.setId(messageDTO.getId());
        messageDTO.setTags(messageDTO.getTags());
        messageDTO.setText(messageDTO.getText());

        return messageDTO;
    }

    public Message toModel(CreateMessageDTO createMessageDTO) {
        return new Message(createMessageDTO.getText(),createMessageDTO.getTags(),createMessageDTO.getAuthor());
    }
}
