package com.kwojciech.zero.ChatZero.controller.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "User NOT found")
public class UserNotFound extends Exception {
}
