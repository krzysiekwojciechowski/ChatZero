package com.kwojciech.zero.ChatZero.controller.dto;

public class UpdateUserDTO {
    private String password;
    private String displayName;

    public UpdateUserDTO() {
    }

    public UpdateUserDTO(String password, String displayName) {
        this.password = password;
        this.displayName = displayName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
