package com.kwojciech.zero.ChatZero.controller.dto;

import com.kwojciech.zero.ChatZero.model.User;

public class CreateMessageDTO {
    private String text;
    private User author;
    private String tags;

    public CreateMessageDTO() {
    }

    public CreateMessageDTO(String text, User author, String tags) {
        this.text = text;
        this.author = author;
        this.tags = tags;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }
}
