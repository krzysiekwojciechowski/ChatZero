package com.kwojciech.zero.ChatZero.controller;

import com.kwojciech.zero.ChatZero.Services.UserServices;
import com.kwojciech.zero.ChatZero.controller.Exception.LoginAlreadyTaken;
import com.kwojciech.zero.ChatZero.controller.Exception.UserNotFound;
import com.kwojciech.zero.ChatZero.controller.Mapper.UserDtoMapper;
import com.kwojciech.zero.ChatZero.controller.dto.CreateUserDTO;
import com.kwojciech.zero.ChatZero.controller.dto.UpdateUserDTO;
import com.kwojciech.zero.ChatZero.controller.dto.UserDTO;
import com.kwojciech.zero.ChatZero.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/user")
public class UserController extends BaseController {
    @Autowired
    UserServices userServices = new UserServices();
    @Autowired
    UserDtoMapper userDtoMapper = new UserDtoMapper();

    @GetMapping
    private List<User> getAllUsers() {
        return userServices.getAllUsers();
    }

    // Codes: 200, 404
    @GetMapping("/{login}")
    private UserDTO getUserByLogin(@PathVariable String login) throws UserNotFound {
        User userByLogin = userServices.getUserByLogin(login);
        return userDtoMapper.toDto(userByLogin);
    }

    /*
        CODES: 200, 400, 409
     */
    @PostMapping
    private ResponseEntity createUser(@RequestBody CreateUserDTO createUserDTO) throws UserNotFound {
        //displayName validation
        if (createUserDTO.getDisplayName() == null || createUserDTO.getDisplayName().isEmpty()) {
            return new ResponseEntity<>("Display Name cannot be empty", HttpStatus.BAD_REQUEST);
        }

        //password validation
        if (createUserDTO.getPassword() == null || createUserDTO.getPassword().isEmpty()) {
            return new ResponseEntity<>("Password cannot be empty", HttpStatus.BAD_REQUEST);
        }

        try {
            User user = userDtoMapper.toModel(createUserDTO);
            User createUSer = userServices.createUser(user);
            return ResponseEntity.ok(userDtoMapper.toDto(createUSer)); // What is going on??????????????????????????????
        } catch (LoginAlreadyTaken loginAlreadyTaken) {
            return new ResponseEntity<>("Cannot create user! Login has been taken!", HttpStatus.CONFLICT);
        }
    }

    @PutMapping("/{login}")
    private ResponseEntity updateUser(@RequestBody UpdateUserDTO updateUserDTO, @PathVariable String login) throws UserNotFound {
        //displayName validation
        if (updateUserDTO.getDisplayName() == null || updateUserDTO.getDisplayName().isEmpty()) {
            return new ResponseEntity<>("Display Name cannot be empty", HttpStatus.BAD_REQUEST);
        }

        //password validation
        if (updateUserDTO.getPassword() == null || updateUserDTO.getPassword().isEmpty()) {
            return new ResponseEntity<>("Password cannot be empty", HttpStatus.BAD_REQUEST);
        }

        User updatedUser = userServices.updateUser(login, updateUserDTO.getPassword(), updateUserDTO.getDisplayName());
        return ResponseEntity.ok(userDtoMapper.toDto(updatedUser));
    }

    @DeleteMapping("/{login}")
    private UserDTO deleteUser(@PathVariable String login) throws UserNotFound {
        User deletedUser = userServices.deleteUser(login);
        return userDtoMapper.toDto(deletedUser);
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "User not found!")
    @ExceptionHandler(UserNotFound.class)
    public void handleUserNotFound() {
    }
}
