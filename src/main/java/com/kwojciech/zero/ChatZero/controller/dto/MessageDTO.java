package com.kwojciech.zero.ChatZero.controller.dto;

import com.kwojciech.zero.ChatZero.model.User;

public class MessageDTO {
    private String text;
    private String tags;
    private int id;
    private User author;

    public MessageDTO() {
    }

    public MessageDTO(String text, String tags, int id, User author) {
        this.text = text;
        this.tags = tags;
        this.id = id;
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }
}
