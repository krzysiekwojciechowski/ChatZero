package com.kwojciech.zero.ChatZero.controller;

import com.kwojciech.zero.ChatZero.Services.MessageService;
import com.kwojciech.zero.ChatZero.controller.Exception.IncorrectMessage;
import com.kwojciech.zero.ChatZero.controller.Exception.MessageNotFound;
import com.kwojciech.zero.ChatZero.controller.Exception.UserNotFound;
import com.kwojciech.zero.ChatZero.controller.dto.CreateMessageDTO;
import com.kwojciech.zero.ChatZero.controller.dto.MessageDTO;
import com.kwojciech.zero.ChatZero.controller.dto.UpdateMessageDTO;
import com.kwojciech.zero.ChatZero.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/message")
public class MessageController extends BaseController {

    @Autowired
    private MessageService messageServices = new MessageService();

    private List<Message> messagesList = new ArrayList<>();

//    @PostConstruct
//    private void init() {
//        List<String> tagsList = Arrays.asList("trip", "city");
//        messagesList.add(new Message("Let's start with Spring", tagsList, new User()));
//    }

    // GET /api/v1/messages?author=jan - zwraca wszystkie wiadomości Jana
    @GetMapping
    public List<Message> getMessages() {
        return messageServices.getAllMessages();
    }

    // GET /api/v1/messages/{id}
    @GetMapping("/{id}")
    public MessageDTO getMessageBtId(@PathVariable int id) throws MessageNotFound {
        return messageServices.getById(id);
    }

    // POST /api/v1/messages
    @PostMapping
    public MessageDTO createMessage(@RequestBody CreateMessageDTO createMessageDTO) throws IncorrectMessage, UserNotFound {
        //validation of author
        if (createMessageDTO.getAuthor() == null || createMessageDTO.getAuthor().getLogin().isEmpty()) {
            throw new IncorrectMessage();
        }

        //validation of text
        if (createMessageDTO.getText() == null || createMessageDTO.getText().isEmpty()) {
            throw new IncorrectMessage();
        }

        return messageServices.create(createMessageDTO);
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Message with ID does not exist")
    @ExceptionHandler(MessageNotFound.class)
    public void handlerMessageNotFound() {
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Message text may not be null or empty")
    @ExceptionHandler(IncorrectMessage.class)
    public void handlerIncorrectMessage() {
    }

    // PUT /api/v1/messages/{id}
    @PutMapping("/{id}")
    public MessageDTO updateMessage(@PathVariable int id, @RequestBody UpdateMessageDTO updateMessageDTO)
            throws IncorrectMessage, MessageNotFound {

        //validation of text
        if (updateMessageDTO.getText() == null || updateMessageDTO.getText().isEmpty()) {
            throw new IncorrectMessage();
        }
        return messageServices.update(id, updateMessageDTO);
    }

    // DELETE /api/v1/messages/{id}
    @DeleteMapping("/{id}")
    public MessageDTO updateMessage(@PathVariable int id) throws MessageNotFound {
        return messageServices.delete(id);
    }
}
