package com.kwojciech.zero.ChatZero.controller.dto;

public class UserDTO {
    private String login;
    private String displayName;
    private String password;

    public UserDTO() {
    }

    public UserDTO(String login, String displayName) {
        this.login = login;
        this.displayName = displayName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
