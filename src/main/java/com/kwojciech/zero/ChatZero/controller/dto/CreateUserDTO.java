package com.kwojciech.zero.ChatZero.controller.dto;

public class CreateUserDTO extends UserDTO {
    private String password;

    public CreateUserDTO() {
    }

    public CreateUserDTO(String password) {
        this.password = password;
    }

    public CreateUserDTO(String login, String password, String displayName) {
        super(login, displayName);
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
