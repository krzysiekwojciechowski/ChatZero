package com.kwojciech.zero.ChatZero.controller.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT, reason = "Login already taken")
public class LoginAlreadyTaken extends Exception {
}
