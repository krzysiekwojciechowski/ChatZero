package com.kwojciech.zero.ChatZero.controller.Mapper;

import com.kwojciech.zero.ChatZero.controller.dto.UserDTO;
import com.kwojciech.zero.ChatZero.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserDtoMapper {

    /**
     * changes USER -> USERDTO
     *
     * @param user
     * @return
     */
    public UserDTO toDto(User user) {
        UserDTO userDTO = new UserDTO();

        userDTO.setDisplayName(user.getDisplayName());
        userDTO.setLogin(user.getLogin());

        return userDTO;
    }

    public User toModel(UserDTO userDTO) {
        User user = new User();

        user.setDisplayName(userDTO.getDisplayName());
        user.setLogin(userDTO.getLogin());
        user.setPassword(userDTO.getPassword());

        return user;
    }
}