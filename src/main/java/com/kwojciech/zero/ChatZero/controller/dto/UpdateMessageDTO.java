package com.kwojciech.zero.ChatZero.controller.dto;

import java.util.List;

public class UpdateMessageDTO {
    private String text;
    private String tags;

    public UpdateMessageDTO() {
    }

    public UpdateMessageDTO(String text, String tags) {
        this.text = text;
        this.tags = tags;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }
}
