package com.kwojciech.zero.ChatZero.controller.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Author and text must not be empty!!!")
public class IncorrectMessage extends Exception {
}
