package com.kwojciech.zero.ChatZero.Services;

import com.kwojciech.zero.ChatZero.controller.Exception.MessageNotFound;
import com.kwojciech.zero.ChatZero.controller.Exception.UserNotFound;
import com.kwojciech.zero.ChatZero.controller.Mapper.MessagesDtoMapper;
import com.kwojciech.zero.ChatZero.controller.dto.CreateMessageDTO;
import com.kwojciech.zero.ChatZero.controller.dto.MessageDTO;
import com.kwojciech.zero.ChatZero.controller.dto.UpdateMessageDTO;
import com.kwojciech.zero.ChatZero.model.Message;
import com.kwojciech.zero.ChatZero.model.User;
import com.kwojciech.zero.ChatZero.repository.MessagesRepository;
import com.kwojciech.zero.ChatZero.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MessageService {

    @Autowired
    private MessagesDtoMapper messagesDtoMapper;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MessagesRepository messagesRepository;

    private List<Message> messagesList = new ArrayList<>();

//    @PostConstruct
//    public void init() {
//        messagesList.add(new Message("Something about SPRING", Arrays.asList("Spring", "JAVA"), new User()));
//    }

    public List<Message> getAllMessages() {
        return messagesRepository.findAll();
    }

    public MessageDTO getById(int id) throws MessageNotFound {
        return messagesRepository.findById(id)
                .map(m -> messagesDtoMapper.toDto(m))
                .orElseThrow(() -> new MessageNotFound());
    }

    public MessageDTO create(CreateMessageDTO createMessageDTO) throws UserNotFound{
        Optional<User> authorOptional = userRepository.findById(createMessageDTO.getAuthor().getLogin());

        if(!authorOptional.isPresent()){
            throw new UserNotFound();
        }

        Message newMessage = new Message(createMessageDTO.getText(), createMessageDTO.getTags(), authorOptional.get());
        Message savedMessage = messagesRepository.save(newMessage);
        return messagesDtoMapper.toDto(savedMessage);
    }

    public MessageDTO update(int id, UpdateMessageDTO updateMessageDTO) throws MessageNotFound {
        Message updatedMesssage = messagesRepository.findById(id)
                .orElseThrow(() -> new MessageNotFound());

        updatedMesssage.setText(updateMessageDTO.getText());
        updatedMesssage.setTags(updateMessageDTO.getTags());

        Message savedMessage = messagesRepository.save(updatedMesssage);

        return messagesDtoMapper.toDto(savedMessage);
    }

    public MessageDTO delete(int id) throws MessageNotFound {
        Message message = messagesRepository.findById(id)
                .orElseThrow(() -> new MessageNotFound());

        messagesRepository.delete(message);
        return messagesDtoMapper.toDto(message);
    }
}
