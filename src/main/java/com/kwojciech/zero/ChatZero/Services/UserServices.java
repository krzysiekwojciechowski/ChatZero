package com.kwojciech.zero.ChatZero.Services;

import com.kwojciech.zero.ChatZero.controller.Exception.LoginAlreadyTaken;
import com.kwojciech.zero.ChatZero.controller.Exception.UserNotFound;
import com.kwojciech.zero.ChatZero.model.User;
import com.kwojciech.zero.ChatZero.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class UserServices {

    @Autowired
    private UserRepository userRepository;

    // metoda inicjalizująca - dodaje domyślnych użytkowników
    @PostConstruct
    private void init() {
        userRepository.save(new User("admin", "admin", "Administrator"));
        userRepository.save(new User("krzys", "iop", "Krzysiek"));
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public User getUserByLogin(String login) throws UserNotFound {
        return userRepository.findAllByLogin(login)
                .orElseThrow(() -> new UserNotFound());
    }

    public User createUser(User user) throws LoginAlreadyTaken {
        if (userRepository.existsByLogin(user.getLogin())) {
            throw new LoginAlreadyTaken();
        } else {
            userRepository.save(user);
            return user;
        }
    }

    public User updateUser(String login, String password, String displayName) throws UserNotFound {
        User existingUser = getUserByLogin(login);

        existingUser.setPassword(password);
        existingUser.setDisplayName(displayName);

        userRepository.save(existingUser);
        return existingUser;
    }

    public User deleteUser(String login) throws UserNotFound {
        User existingUser = getUserByLogin(login);
        userRepository.delete(existingUser);
        return existingUser;
    }
}
